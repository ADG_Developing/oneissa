#include <iostream>
#include <string>
#include <time.h>
#include <Windows.h>

#include <glew.h>

#include "Camera.h"
#include "GLSLShader.h"
#include "Mesher.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <glut.h>
#endif

GLuint cubeShader;
GLuint cubeShader2;

Mesher* mesher = new Mesher();

Camera g_camera;
bool g_key[256];
bool g_shift_down = false;
bool g_fps_mode = false;
int g_viewport_width = 0;
int g_viewport_height = 0;
bool g_mouse_left_down = false;
bool g_mouse_right_down = false;

// Movement settings
const float g_translation_speed = 0.05;
const float g_rotation_speed = M_PI / 180 * 0.2;

//  The number of frames
int frameCount = 0;

//  Number of frames per second
float fps = 0;

//  currentTime - previousTime is the time elapsed
//  between every call of the Idle function
int currentTime = 0, previousTime = 0;

void calculateFPS()
{
	//  Increase frame count
	frameCount++;

	//  Get the number of milliseconds since glutInit called 
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	currentTime = glutGet(GLUT_ELAPSED_TIME);

	//  Calculate time passed
	int timeInterval = currentTime - previousTime;

	if (timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps = frameCount / (timeInterval / 1000.0f);

		//  Set time
		previousTime = currentTime;

		//  Reset frame count
		frameCount = 0;
	}
}

void renderScene(void) 
{
	glutSwapBuffers();
}

void gameLoop()
{
	glClearColor(135.0 / 255.0, 206.0 / 255.0, 250.0 / 255.0, 1.0); //clear the screen to skyblue
	glClearDepth(1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
	glLoadIdentity();
	//glScalef(0.25f, 0.25f, 0.25f);

	std::string s = std::to_string(fps);
	std::string s2 = "FPS: " + s;

	glutSetWindowTitle(s2.c_str());

	mesher->shader(cubeShader, cubeShader2, g_camera);
	mesher->draw();

	//Universe::physics(g_camera);

	calculateFPS();

	g_camera.Refresh();
	glutSwapBuffers();
}

void reshapeFunc(int w, int h) {
	g_viewport_width = w;
	g_viewport_height = h;

	glViewport(0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
	glMatrixMode(GL_PROJECTION); //set the matrix to projection

	glLoadIdentity();
	gluPerspective(90, (GLfloat)w / (GLfloat)h, 0.1, 100.0); //set the perspective (angle of sight, width, height, ,depth)
	glMatrixMode(GL_MODELVIEW); //set the matrix back to model
}

void processNormalKeys(unsigned char key, int x, int y)
{
	if (key == 27) {
		exit(0);
	}

	if (key == 'r') {
		g_fps_mode = !g_fps_mode;

		if (g_fps_mode) {
			glutSetCursor(GLUT_CURSOR_NONE);
			glutWarpPointer(g_viewport_width / 2, g_viewport_height / 2);
		}
		else {
			glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
		}
	}

	if (glutGetModifiers() == GLUT_ACTIVE_SHIFT) {
		g_shift_down = true;
	}
	else {
		g_shift_down = false;
	}

	g_key[key] = true;
}

void processSpecialKeys(int key, int x, int y) 
{

}

void processKeysUp(unsigned char key, int x, int y)
{
	g_key[key] = false;
}

void timerFunc(int value)
{
	if (g_fps_mode) {
		if (g_key['w'] || g_key['W']) {
			g_camera.Move(g_translation_speed);

			float x;
			float y;
			float z;

			g_camera.GetPos(x, y, z);
		}
		if (g_key['s'] || g_key['S']) {
			g_camera.Move(-g_translation_speed);
		}
		if (g_key['a'] || g_key['A']) {
			g_camera.Strafe(g_translation_speed);
		}
		if (g_key['d'] || g_key['D']) {
			g_camera.Strafe(-g_translation_speed);
		}
		if (g_mouse_left_down) {
			//g_camera.Fly(-g_translation_speed);

			float px = 0;
			float py = 0;
			float pz = 0;

			g_camera.GetPos(px, py, pz);

			//Player::placeBlock(1, std::round((int)px), std::round((int)py), std::round((int)pz));
		}
		if (g_mouse_right_down) {
			//g_camera.Fly(g_translation_speed);
		}

		if (g_key['='])
		{
			std::cout << "Sup";
		}
	}

	glutTimerFunc(1, timerFunc, 0);
}

void mouseFunc(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		if (button == GLUT_LEFT_BUTTON) {
			g_mouse_left_down = true;
		}
		else if (button == GLUT_RIGHT_BUTTON) {
			g_mouse_right_down = true;
		}
	}
	else if (state == GLUT_UP) {
		if (button == GLUT_LEFT_BUTTON) {
			g_mouse_left_down = false;
		}
		else if (button == GLUT_RIGHT_BUTTON) {
			g_mouse_right_down = false;
		}
	}
}

void mouseMotionFunc(int x, int y)
{
	// This variable is hack to stop glutWarpPointer from triggering an event callback to Mouse(...)
	// This avoids it being called recursively and hanging up the event loop
	static bool just_warped = false;

	if (just_warped) {
		just_warped = false;
		return;
	}

	if (g_fps_mode) {
		int dx = x - g_viewport_width / 2;
		int dy = y - g_viewport_height / 2;

		if (dx) {
			g_camera.RotateYaw(g_rotation_speed*dx);
		}

		if (dy) {
			g_camera.RotatePitch(g_rotation_speed*-dy);
		}

		glutWarpPointer(g_viewport_width / 2, g_viewport_height / 2);

		just_warped = true;
	}
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(640, 480);
	glutCreateWindow("Oneissa - Outerspace Madness");

	glewInit();

	GLSLShader* shaderManager = new GLSLShader();

	cubeShader = shaderManager->LoadShader("../data/shaders/Cube.vert", "../data/shaders/Cube.frag");
	//cubeShader2 = shaderManager->LoadShader("../data/shaders/atmos.vert", "../data/shaders/atmos.frag");
	glUseProgram(cubeShader);
	//glUseProgram(cubeShader2);

	glEnable(GL_DEPTH_FUNC);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_TRUE);
	glCullFace(GL_BACK);

	mesher->init();
    mesher->createTerrain2();
	//Planet::biomeLoader(0,0,0);
	mesher->buildVBO();

	//Universe::init();

	glutIgnoreKeyRepeat(1);

	glutDisplayFunc(renderScene);
	glutIdleFunc(gameLoop);
	glutReshapeFunc(reshapeFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
	glutPassiveMotionFunc(mouseMotionFunc);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(processKeysUp);
	glutIdleFunc(gameLoop);

	glutTimerFunc(1, timerFunc, 0);
	glutMainLoop();

	return 0;
}