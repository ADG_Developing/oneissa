#include <glm\glm.hpp>

using namespace std;

class CoordSystem
{
public:
	int cubeCoords[256][256][256];

	void addCoords(int id, int x, int y, int z, int type);
	int getBlock(int x, int y, int z);
};