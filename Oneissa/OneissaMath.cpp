#include "OneissaMath.h"

glm::vec2 OneissaMath::calculateUV(int x, int y)
{
	float outx = 0;
	float outy = 0;

	if (x == 0)
	{
		outx = 0;
	}
	else
	{
		//outx = 0.00f - (1.00f / x);
		outx = float(x) / float(3);
	}

	if (y == 0)
	{
		outy = 0;
	}
	else
	{
		//outy = 0.00f - (1.00f / y);
		outy = float(y) / float(3);
	}

	return glm::vec2(outx, outy);
}