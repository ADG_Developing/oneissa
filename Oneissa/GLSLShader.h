#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <glew.h>

#define GLSHADER_H

class GLSLShader
{
public:
	std::string readFile(const char *filePath);
	GLuint LoadShader(const char *vertex_path, const char *fragment_path);
};