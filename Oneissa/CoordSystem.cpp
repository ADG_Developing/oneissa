#include "CoordSystem.h"

void CoordSystem::addCoords(int id, int x, int y, int z, int type)
{
	cubeCoords[x][y][z] = type;
}

int CoordSystem::getBlock(int x, int y, int z)
{
	return cubeCoords[x][y][z];
}