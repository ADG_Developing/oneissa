biome = {
    name = "Plains",
    noise = "perlin3D",
    noiseSeed = {1, 1, 5, 2, 10, 50},
    seed = {1, 1, 2, 3, 5, 10, 20}
}
